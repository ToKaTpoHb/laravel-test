<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker, $settings) {
    $user_id = $settings['user_id'];
    return [
        'title' => $faker->words(5, true),
        'content' => $faker->paragraph(5),
        'user_id' => $user_id,
    ];
});

