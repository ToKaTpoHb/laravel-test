<h2><a href="{{route('posts.show', ['id' => $post->id])}}" style="color: black;">{{$post->title}}</a></h2>
<p>
    @foreach($post->categories as $category)
        <a href="{{route('posts.list')}}?category={{$category->id}}" class="badge">{{$category->name}}</a>
    @endforeach
</p>
<br>
@if($post->image)
    <img src="{{$post->image_url}}" alt="Post preview image" style="max-width: 250px;">
    <br><br>
@endif
<p>{{$post->createdBy->name}} wrote it {{$post->created_at->diffForHumans()}}</p>