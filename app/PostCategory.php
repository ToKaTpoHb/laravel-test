<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PostCategory
 * @package App
 * @property string name Category name
 */
class PostCategory extends Model
{
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
